//
//  ViewController.swift
//  StickerDev
//
//  Created by Ihwan on 11/09/21.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var stickerCollectionView: UICollectionView!
    @IBOutlet weak var stickerActionButton: UIButton!
    
    var dataStickerPack: [Pack] = []
    var urlStickerPack: [String] = [] //["https://filediffchecker.com/growapp/stick_pack_1/01.GIF", "https://filediffchecker.com/growapp/stick_pack_1/02.GIF", "https://filediffchecker.com/growapp/stick_pack_1/03.GIF", "https://filediffchecker.com/growapp/stick_pack_1/04.GIF", "https://filediffchecker.com/growapp/stick_pack_1/05.GIF", "https://filediffchecker.com/growapp/stick_pack_1/06.GIF", "https://filediffchecker.com/growapp/stick_pack_1/07.GIF", "https://filediffchecker.com/growapp/stick_pack_1/08.GIF", "https://filediffchecker.com/growapp/stick_pack_1/09.GIF", "https://filediffchecker.com/growapp/stick_pack_1/10.GIF", "https://filediffchecker.com/growapp/stick_pack_1/11.GIF", "https://filediffchecker.com/growapp/stick_pack_1/12.GIF", "https://filediffchecker.com/growapp/stick_pack_1/13.GIF", "https://filediffchecker.com/growapp/stick_pack_1/14.GIF", "https://filediffchecker.com/growapp/stick_pack_1/15.GIF", "https://filediffchecker.com/growapp/stick_pack_1/16.GIF", "https://filediffchecker.com/growapp/stick_pack_1/17.GIF", "https://filediffchecker.com/growapp/stick_pack_1/18.GIF", "https://filediffchecker.com/growapp/stick_pack_1/19.GIF", "https://filediffchecker.com/growapp/stick_pack_1/20.GIF", "https://filediffchecker.com/growapp/stick_pack_1/21.GIF", "https://filediffchecker.com/growapp/stick_pack_1/22.GIF", "https://filediffchecker.com/growapp/stick_pack_1/23.GIF", "https://filediffchecker.com/growapp/stick_pack_1/24.GIF"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stickerCollectionView.delegate = self
        stickerCollectionView.dataSource = self
        
        getDataFromApi()
        
    }
    
    func getDataFromApi(){
        let stickerURL = "https://filediffchecker.com/growapp/stickers.php"
        let session = URLSession.shared
        let url = URL(string: stickerURL)!
        
        let task = session.dataTask(with: url) { data, response, error in
            
            if error != nil || data == nil {
                print("Client error!")
                return
            }
            
            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error!")
                return
            }
            
            do {
                let json = try JSONDecoder().decode(Root.self, from: data!)
                print(json)
                let urls = self.getStickerURL(folder: json.packs.first?.folder ?? "", howMany: json.packs.first?.howmany.toInt() ?? 0, type: json.packs.first?.type ?? "GIF")
                self.urlStickerPack.append(contentsOf: urls)
                DispatchQueue.main.async {
                    self.stickerCollectionView.reloadData()
                }
            } catch {
                print("JSON error: \(error.localizedDescription)")
            }
            
        }.resume()
    }
    
    func getStickerURL(folder: String, howMany: Int, type: String) -> [String]{
        var urls: [String] = []
        for number in 1...howMany {
            let filename = String(format: "%02d", number)
            let baseURLSticker = "https://filediffchecker.com/growapp/\(folder)/\(filename).\(type)"
            urls.append(baseURLSticker)
            
        }
        print(urls)
        return urls
    }
    
    private func store(image: UIImage, forKey key: String) {
        if let filePath = filePath(forKey: key) {
            do  {
                try image.pngData()?.write(to: filePath,
                                           options: .atomic)
            } catch let err {
                print("Saving file resulted in error: ", err)
            }
        }
    }
    
    private func retrieveImage(forKey key: String) -> UIImage? {
        
        if let filePath = self.filePath(forKey: key),
           let fileData = FileManager.default.contents(atPath: filePath.path),
           let image = UIImage(data: fileData) {
            return image
        }
        
        return nil
    }
    
    private func filePath(forKey key: String) -> URL? {
        let fileManager = FileManager.default
        guard let documentURL = fileManager.urls(for: .documentDirectory,
                                                 in: FileManager.SearchPathDomainMask.userDomainMask).first else { return nil }
        
        return documentURL.appendingPathComponent(key + ".png")
    }
    
    func saveImage(ids: [String], datas: [Data]){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        for (id, data) in zip(ids, datas) {
            let stickerEntity = NSEntityDescription.insertNewObject(forEntityName: "StickerEntity", into: context) as! StickerEntity
            stickerEntity.id = id
            stickerEntity.image = data
        }
        
        
        do{
            try context.save()
        }catch let error{
            print(error.localizedDescription)
        }
    }
    
    func retrieveImage(){
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "StickerEntity")
        do{
            let photos = try context.fetch(fetchRequest) as? [StickerEntity]
            print("coredata \(photos?.count)")
        } catch let error{
            print(error.localizedDescription)
        }
    }
    
    //MARK: Action
    @IBAction func stickerActionButtonTapped(_ sender: Any) {
        let datas = getImageData(urls: urlStickerPack)
        saveImage(ids: urlStickerPack, datas: datas)
        retrieveImage()
    }
    
    func getImageData(urls: [String]) -> [Data]{
        var datas: [Data] = []
        for urlString in urls {
            let url = URL(string: urlString)!
            if let data = try? Data(contentsOf: url) {
                datas.append(data)
            }
        }
        return datas
    }
}

extension ViewController: UICollectionViewDelegate{
    
}

extension ViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return urlStickerPack.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "stickerCell", for: indexPath) as! StickerCollectionViewCell
        cell.configure(url: urlStickerPack[indexPath.row])
        return cell
    }
    
    
}

extension ViewController: UICollectionViewDelegateFlowLayout{
    
}

struct Root: Decodable{
    let packs: [Pack]
}

struct Pack: Decodable{
    let type: String
    let howmany: String
    let folder: String
}

extension String {
    //Converts String to Int
    public func toInt() -> Int? {
        if let num = NumberFormatter().number(from: self) {
            return num.intValue
        } else {
            return nil
        }
    }
    
    //Converts String to Double
    public func toDouble() -> Double? {
        if let num = NumberFormatter().number(from: self) {
            return num.doubleValue
        } else {
            return nil
        }
    }
    
    /// EZSE: Converts String to Float
    public func toFloat() -> Float? {
        if let num = NumberFormatter().number(from: self) {
            return num.floatValue
        } else {
            return nil
        }
    }
    
    //Converts String to Bool
    public func toBool() -> Bool? {
        return (self as NSString).boolValue
    }
}
