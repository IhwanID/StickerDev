//
//  StickerCollectionViewCell.swift
//  StickerDev
//
//  Created by Ihwan on 11/09/21.
//

import UIKit

class StickerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var stickerImageView: UIImageView!
 
    func configure(url: String){
        stickerImageView.load(url: URL(string: url)!)
//        DispatchQueue.global(qos: .background).async {
//                if let savedImage = self.retrieveImage(forKey: "\(url)") {
//                    DispatchQueue.main.async {
//                        self.stickerImageView.image = savedImage
//                    }
//                }
//            }
    }
    
    private func store(image: UIImage, forKey key: String) {
        if let filePath = filePath(forKey: key) {
                        do  {
                            try image.pngData()?.write(to: filePath,
                                                        options: .atomic)
                        } catch let err {
                            print("Saving file resulted in error: ", err)
                        }
                    }
    }
    
    private func retrieveImage(forKey key: String) -> UIImage? {
        
            if let filePath = self.filePath(forKey: key),
                      let fileData = FileManager.default.contents(atPath: filePath.path),
                      let image = UIImage(data: fileData) {
                      return image
                  }
        
        return nil
    }
    
    private func filePath(forKey key: String) -> URL? {
        let fileManager = FileManager.default
        guard let documentURL = fileManager.urls(for: .documentDirectory,
                                                in: FileManager.SearchPathDomainMask.userDomainMask).first else { return nil }
        
        return documentURL.appendingPathComponent(key + ".png")
    }
}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
