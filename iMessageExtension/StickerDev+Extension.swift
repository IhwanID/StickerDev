//
//  StickerDev+Extension.swift
//  iMessageExtension
//
//  Created by Ihwan on 11/09/21.
//

import Foundation
import Messages

extension MSSticker {
    convenience init(called description: String, from filename: String) {
        let path = Bundle.main.path(forResource: filename, ofType: "png")! // Make sure the specified file exists; otherwise, you’ll get a runtime error.
        let url = URL(fileURLWithPath: path)
        try! self.init(contentsOfFileURL: url, localizedDescription: description) // Unsafe
    }
}
