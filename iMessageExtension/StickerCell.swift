//
//  StickerCell.swift
//  iMessageExtension
//
//  Created by Ihwan on 11/09/21.
//

import Foundation
import Messages

class StickerCell: UICollectionViewCell {

    @IBOutlet weak var stickerView: MSStickerView!
    
}
